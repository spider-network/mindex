[![Gem Version](https://badge.fury.io/rb/mindex.svg)](https://badge.fury.io/rb/mindex)
# Mindex

This gem provides functionality to build elasticsearch indices.

## Installation

Add the following line to your Gemfile:
```ruby
gem 'mindex'
```

And then execute:
```
$ bundle
```

## Example

### Index declaration
```ruby
module Index
  class Event
    include Mindex::Index

    def self.scroll(options = {})
      query = Event
      query = query.where('"updated_at" >= ?', options[:started_at]) if options[:started_at]
      query.find_in_batches do |events|
        yield index_data(events)
      end
    end

    def self.fetch(ids)
      Event.where(id: ids).map do |event|
        index_data(event)
      end
    end

    private

    def index_data(events)
      [events].flatten.map do |event|
        {
          id:         event.id,
          name:       event.name,
          updated_at: event.updated_at,
          created_at: event.created_at
        }
      end
    end
  end
end
```

### Usage
recreate index
```ruby
Event.recreate_index
```

reindex all items
```ruby
Event.reindex
```

index specific items
```ruby
Event.reindex_items([2903, 2016])
```

drop items
```ruby
Event.drop_items([29, 3])
```

get item from elasticsearch
```ruby
Event.es.get(index: Event.index_alias, id: 2903)
```

## Contributing
Please create an issue or merge request
