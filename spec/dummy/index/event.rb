# frozen_string_literal: true

module Index
  class Event
    include Mindex::Index

    def self.reset_settings!
      @connection_settings  = nil
      @index_settings       = nil
      @index_mappings       = nil
      @index_prefix         = nil
      @index_label          = nil
      @index_num_threads    = nil
    end

    def self.scroll(_options = {})
      DB[:events].select.each_slice(500) do |events|
        yield events
      end
    end

    def self.fetch(ids)
      DB[:events].where(id: ids).all
    end
  end
end
