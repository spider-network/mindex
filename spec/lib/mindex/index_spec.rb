# frozen_string_literal: true

require 'dummy/index/event'

describe Mindex::Index do # rubocop:disable Metrics/BlockLength
  let(:index_klass) { Index::Event }

  before do
    index_klass.reset_settings!
  end

  describe '.index_alias' do
    it 'returns the alias name of the index' do
      expect(index_klass.index_alias).to eq 'events'
    end

    context 'when prefix is definde' do
      let(:prefix) { 'foobar' }
      before { index_klass.index_prefix(prefix) }

      it 'prefixes the alias name' do
        expect(index_klass.index_alias).to eq "#{prefix}_events"
      end
    end

    context 'when label is definde' do
      let(:label) { 'competition' }
      before { index_klass.index_label(label) }

      it 'uses the label instead of the tableize class name' do
        expect(index_klass.index_alias).to eq label
      end
    end
  end

  describe '.doc_type' do
    it 'uses the tableize class name' do
      expect(index_klass.doc_type).to eq index_klass.name.demodulize.tableize
    end

    context 'when label is definde' do
      let(:label) { 'competition' }
      before { index_klass.index_label(label) }

      it 'uses the label instead of the tableize class name' do
        expect(index_klass.doc_type).to eq label
      end
    end
  end

  describe '.index_name' do
    context 'when index does not exists' do
      it do
        expect(index_klass.index_name).to be_nil
      end
    end

    context 'when index exists' do
      before do
        @index_name = index_klass.index_create
      end

      it 'returns the index name' do
        expect(index_klass.index_name).to eq @index_name
      end
    end
  end

  describe '.index_refresh' do
    context 'when index does not exists' do
      it do
        expect { index_klass.index_refresh }.not_to raise_error
      end
    end

    context 'when index exists' do
      before { index_klass.index_create }

      it do
        expect { index_klass.index_refresh }.not_to raise_error
      end
    end
  end

  describe '.index_exist?' do
    context 'when the index does not exists' do
      it do
        expect(index_klass.index_exist?).to be false
      end
    end

    context 'when the index exists' do
      before { index_klass.index_create }

      it do
        expect(index_klass.index_exist?).to be true
      end
    end
  end

  describe '.connection_settings' do
    let(:connection_settings) { { foo: :bar, foobar: :baz, qux: :quux } }

    before do
      allow(Mindex::Elasticsearch).to receive(:connect)
      index_klass.connection_settings(connection_settings)
    end

    it 'overwrites the connection settings' do
      index_klass.elasticsearch
      expect(Mindex::Elasticsearch).to have_received(:connect).with(connection_settings)
    end
  end

  describe '.index_config' do
    let(:settings) { { foo: :bar } }
    let(:mapping) { { bar: :foo } }

    before do
      @indices = instance_double(Elasticsearch::API::Indices::IndicesClient, create: nil)
      allow(::Elasticsearch::Client).to receive(:new).and_return(instance_double(Elasticsearch::Transport::Client, indices: @indices))

      index_klass.index_config(settings: settings, mappings: mapping)
    end

    it 'creates the index with the given settings and mappings' do
      index_klass.index_create(move_or_create_index_alias: false)
      expect(@indices).to have_received(:create).with(hash_including(index: anything, body: { settings: settings, mappings: mapping }))
    end
  end

  describe '.reindex' do
    let(:event_1) { { 'id' => 1, 'name' => 'BerlinMan' } }
    let(:event_2) { { 'id' => 2, 'name' => 'Leipziger Triathlon' } }

    before do
      DB.run 'CREATE TABLE events (id integer primary key autoincrement, name varchar(255))'
      DB[:events].insert(event_1)
      DB[:events].insert(event_2)
    end

    it 'updates the entities' do
      index_klass.reindex
      expect(index_klass.es.get(index: index_klass.index_alias, id: event_1['id'])['_source']).to eq event_1
      expect(index_klass.es.get(index: index_klass.index_alias, id: event_2['id'])['_source']).to eq event_2
      DB[:events].where(id: event_1['id']).update(name: 'BerlinMan 2018')
      DB[:events].where(id: event_2['id']).update(name: 'Leipziger Triathlon 2018')
      index_klass.reindex
      expect(index_klass.es.get(index: index_klass.index_alias, id: event_1['id'])['_source']['name']).to eq 'BerlinMan 2018'
      expect(index_klass.es.get(index: index_klass.index_alias, id: event_2['id'])['_source']['name']).to eq 'Leipziger Triathlon 2018'
    end
  end

  describe '.reindex_items' do
    let(:event_1) { { 'id' => 1, 'name' => 'BerlinMan' } }
    let(:event_2) { { 'id' => 2, 'name' => 'Paderborner Triathlon' } }

    before do
      index_klass.index_create
      DB.run 'CREATE TABLE events (id integer primary key autoincrement, name varchar(255))'
      DB[:events].insert(event_1)
      DB[:events].insert(event_2)
    end

    it 'updates the entities' do
      index_klass.reindex_items([event_1['id'], event_2['id']])
      expect(index_klass.es.get(index: index_klass.index_alias, id: event_1['id'])['_source']['name']).to eq 'BerlinMan'
      expect(index_klass.es.get(index: index_klass.index_alias, id: event_2['id'])['_source']['name']).to eq 'Paderborner Triathlon'
    end
  end

  describe '.drop_items' do
    let(:event) { { 'id' => 1, 'name' => 'BerlinMan' } }

    before do
      DB.run 'CREATE TABLE events (id integer primary key autoincrement, name varchar(255))'
      DB[:events].insert(event)
      index_klass.reindex
    end

    it 'drops the given entities' do
      expect { index_klass.es.get(index: index_klass.index_alias, id: event['id']) }.not_to raise_error

      index_klass.drop_items(event['id'])

      expect do
        index_klass.es.get(index: index_klass.index_alias, id: event['id'])
      end.to raise_error(Elasticsearch::Transport::Transport::Errors::NotFound)
    end
  end

  describe '.recreate_index' do
    let(:event) { { 'id' => 1, 'name' => 'BerlinMan' } }

    before do
      DB.run 'CREATE TABLE events (id integer primary key autoincrement, name varchar(255))'
      DB[:events].insert(event)
    end

    it 'inserts the entities in a new index' do
      index_name = index_klass.recreate_index
      expect(index_klass.es.get(index: index_name, id: event['id'])['_source']).to eq event
    end
  end
end
