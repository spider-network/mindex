# frozen_string_literal: true

describe Mindex do
  it 'defines a version number' do
    expect(described_class::VERSION).to eq '0.0.0'
  end
end
